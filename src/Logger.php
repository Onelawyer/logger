<?php
/**
 * ANGERRO Logger 0.2
 *
 * Example to use:
 *
 * //use namespace
 * use ANGERRO;
 *
 * //include logger script
 * include 'Logger.php';
 *
 * // OPTIONS:
 * // to enable trace if you want:
 * ANGERRO\Logger::$Trace = 'ON';
 *
 * // to set path of log file if you want to write data in log file:
 * ANGERRO\Logger::$LogPath = $_SERVER['DOCUMENT_ROOT'].'/log_path/log.log';
 *
 * //to disable log:
 * ANGERRO\Logger::$Mode = 'OFF';
 *
 * // USING
 * // to write data in log file:
 * ANGERRO\Logger::Log($SomeData);
 *
 * // to debug data in browser console:
 * ANGERRO\Logger::ConsoleLog($SomeData);
 *
 * // to debug data in browser console with headline:
 * ANGERRO\Logger::ConsoleLog($SomeData, 'SomeData');
 *
 * // to write data in <pre> tag:
 * ANGERRO\Logger::Pre($SomeData);
 *
 */

namespace ANGERRO;


class Logger{

    /**
     * Path of log file
     * @var
     */
    public static $LogPath = false;

    /**
     * Logger mode
     * @var string
     */
    public static $Mode = 'ON';

    /**
     * Write or not trace data to log
     * @var string
     */
    public static $Trace = 'OFF';


    private function __clone(){

    }

    private function __construct(){

    }

    /**
     * Create log string for Log function
     * @param $data
     * @return bool|string
     */
    protected static function MakeLogFileString($data){
        $log = false;
        if (self::$Mode == 'ON') {
            $log = '============================='.PHP_EOL;
            $log .= 'DateTime: '.date('d.m.Y H:i:s').PHP_EOL;
            $log .= 'Log data: '.PHP_EOL;
            if ($data) {
                if (is_object($data) || is_array($data)) {
                    $log .= print_r($data, true);
                } else {
                    $log .= $data . PHP_EOL;
                }
            }
            if (self::$Trace == 'ON') {
                $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
                $log .= 'Trace data:'.PHP_EOL;
                foreach ($backtrace as $key => $el){
                    if ($key > 0) {
                        $log .= '#' . $key . ' ' . $el['class'].$el['type'].$el['function'] . '() called at [' . $el['file'] . ':' . $el['line'] . ']' . PHP_EOL;
                    }
                }
            }
            $log .= '============================='.PHP_EOL.PHP_EOL;
        }
        return $log;
    }

    /**
     * Create log string for ConsoleLog and Pre functions
     * @param $data
     * @return bool|array
     */
    protected static function MakeLogString($data){
        $log = false;
        if (self::$Mode == 'ON') {
            if (self::$Trace == 'ON') {
                $log .= 'Log data: ' . PHP_EOL;
            }
            if ($data) {
                if (is_object($data) || is_array($data)) {
                    $log .= print_r($data, true);
                } else {
                    $log .= $data . PHP_EOL;
                }
            }
            if (self::$Trace == 'ON') {
                $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
                $log .= 'Trace data:'.PHP_EOL;
                foreach ($backtrace as $key => $el){
                    if ($key > 0) {
                        $log .= '#' . $key . ' ' . $el['class'].$el['type'].$el['function'] . '() called at [' . $el['file'] . ':' . $el['line'] . ']' . PHP_EOL;
                    }
                }
            }
        }
        return $log;
    }

    /**
     * Write log data to log file
     * @param $data
     */
    public static function Log($data = ''){
        $log = self::MakeLogFileString($data);
        if ($log && self::$LogPath){
            file_put_contents(self::$LogPath, $log, FILE_APPEND);
        }
    }

    /**
     * Write log data to browser console
     * @param string $data
     * @param string $group
     */
    public static function ConsoleLog($data = '', $group = ''){
        $log = self::MakeLogString($data);
        if ($log) {
            $log = json_encode($log);
            $group_prefix = $group_postfix = '';
            if ($group){
                $group_prefix = 'console.group("'.$group.'");';
                $group_postfix = 'console.groupEnd();';
            }
            echo <<< EOT
                <script>
                    $group_prefix console.log($log); $group_postfix
                </script>    
EOT;
        }
    }

    /**
     * Write log data in <pre> tag
     * @param string $data
     */
    public static function Pre($data = ''){
        $log = self::MakeLogString($data);
        if ($log){
            echo <<< EOT
                <pre>$log</pre>
EOT;
        }
    }
}