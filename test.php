<?php
/**
 * Test Logger
 */
use ANGERRO;

include 'src/Logger.php';


function a() {
    b();
}

function b() {
    c();
}

function c(){

    $TestDataArray = ['1'=>'test', 'key'=>'data'];
    $TestDataString = 'test string <br> data 2';

    $TestDataObject = new stdClass();
    $TestDataObject->test_data = array('Kalle', 'Ross', 'Felipe');


    //test console log:
    ANGERRO\Logger::ConsoleLog($TestDataString, '$TestDataString');
    ANGERRO\Logger::$Trace = 'ON';
    ANGERRO\Logger::ConsoleLog($TestDataArray, '$TestDataArray');
    ANGERRO\Logger::$Trace = 'OFF';
    ANGERRO\Logger::ConsoleLog($TestDataObject);
    ANGERRO\Logger::$Mode = 'OFF';
    ANGERRO\Logger::ConsoleLog($TestDataString);
    ANGERRO\Logger::$Mode = 'ON';

    //test write log in file

    ANGERRO\Logger::$LogPath = $_SERVER['DOCUMENT_ROOT'].'/files/log.log';
    ANGERRO\Logger::Log($TestDataArray);
    ANGERRO\Logger::$Trace = 'ON';
    ANGERRO\Logger::Log($TestDataString);
    ANGERRO\Logger::$Trace = 'OFF';
    ANGERRO\Logger::Log($TestDataObject);
    ANGERRO\Logger::$Mode = 'OFF';
    ANGERRO\Logger::ConsoleLog($TestDataObject);
    ANGERRO\Logger::$Mode = 'ON';

    //test pre

    ANGERRO\Logger::Pre($TestDataArray);
    ANGERRO\Logger::Pre($TestDataString);
    ANGERRO\Logger::$Trace = 'ON';
    ANGERRO\Logger::Pre($TestDataObject);

}

//a();
