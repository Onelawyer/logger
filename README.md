# Логгер

Класс для логирования данных. Позволяет производить логирование данных 

в **файл на сервере**: 

![File-Log](images/file-log.jpg)

либо в **консоль браузера**: 

![Console-Log](images/console-log.jpg) 

## Документация

- **Подключение** 
    ```
    //подключаем пространство имен:
    use ANGERRO;
    //подключаем скрипт логгера:
    include 'src/Logger.php'; 
    ```

- **Опции** 
    ```
    //если в логе необходима обратная трассировка:
    ANGERRO\Logger::$Trace = 'ON';
    
    //установка пути до файла лога, если будем писать логи в файл:
    ANGERRO\Logger::$LogPath = $_SERVER['DOCUMENT_ROOT'].'/log_path/log.log';
    
    //если хотим отключить логирование:
    ANGERRO\Logger::$Mode = 'OFF';
    ```

- **Использование**
    ```
    //запись данных переменной $SomeData в файл лога $LogPath (настроенный в опциях):
    ANGERRO\Logger::Log($SomeData);
    
    //отображение данных переменной $SomeData в консоли браузера:
    ANGERRO\Logger::ConsoleLog($SomeData);
    ```

## Примеры использования

- **Вывод лога в консоль**

    ```
    use ANGERRO;
    include 'src/Logger.php';
    
    $TestLogData = ['1'=>'test', 'key'=>'data'];
    ANGERRO\Logger::ConsoleLog($TestLogData);
    ```

- **Вывод лога в консоль с трассировкой**

    ```
    use ANGERRO;
    include 'src/Logger.php';
    
    $TestLogData = ['1'=>'test', 'key'=>'data'];
    ANGERRO\Logger::$Trace = 'ON';
    ANGERRO\Logger::ConsoleLog($TestLogData);
    ```
    
- **Вывод лога с заголовком в консоль**

    ```
    use ANGERRO;
    include 'src/Logger.php';
    
    $TestLogData = ['1'=>'test', 'key'=>'data'];
    ANGERRO\Logger::ConsoleLog($TestLogData, 'TestLogData');
    ```

- **Вывод лога в файл**

    ```
    use ANGERRO;
    include 'src/Logger.php';
    
    $TestLogData = ['1'=>'test', 'key'=>'data'];
    ANGERRO\Logger::$LogPath = $_SERVER['DOCUMENT_ROOT'].'/files/log.log';
    ANGERRO\Logger::Log($TestLogData);
    ```

- **Вывод лога в файл с трассировкой**

    ```
    use ANGERRO;
    include 'src/Logger.php';
    
    $TestLogData = ['1'=>'test', 'key'=>'data'];
    ANGERRO\Logger::$LogPath = $_SERVER['DOCUMENT_ROOT'].'/files/log.log';
    ANGERRO\Logger::$Trace = 'ON';
    ANGERRO\Logger::Log($TestLogData);
    ```
    
- **Вывод лога в тег 'pre'**

    ```
    use ANGERRO;
    include 'src/Logger.php';
    
    $TestLogData = ['1'=>'test', 'key'=>'data'];
    ANGERRO\Logger::Pre($TestLogData); 
    ```
    
- **Отключение логирования**  

    ```
    use ANGERRO;
    include 'src/Logger.php';
    
    ANGERRO\Logger::$Mode = 'OFF'; 
    ```
